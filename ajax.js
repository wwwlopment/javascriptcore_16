function sendByGETMethod() {

  var arr = getData();
  if (!arr) {
    return false;
  }

  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/formGet?fName=' + arr.fName + '&lName=' + arr.lName + '&age=' + arr.age + '&address=' + arr.address);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.send();
  xhr.onload = function() {
    console.log(xhr.responseText);
  }
}

function sendByPOSTMethod() {
    var arr = getData();
    if (!arr) {
      return false;
    }
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/formPost');
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.send(arr.toString());
    xhr.onload = function() {
      console.log(xhr.responseText);
    }

}

function getData() {
  var data = document.getElementsByClassName('input3');
  var arr = new Array();
  for (var i = 0; i < data.length; i++) {
    arr[data[i].getAttribute('name')] = data[i].value;
    if (data[i].getAttribute('name') === 'age' && (data[i].value < 1 || data[i].value > 100)) {
      data[i].setAttribute("style", "color: red;");
      document.getElementsByClassName('focus-input3')[0].innerHTML = "Помилка! Невірне значення!";
      return false;
    }
  }
  return arr;
}
